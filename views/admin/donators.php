<? $page = "donators";
require_once __DIR__ .'/../../lib/Helpers/admin-helper.php';
require_once __DIR__ .'/../../lib/Helpers/include-linkbuilder.php';
if(!$Class->part->is_logged_in()) {
    $Class->part->redirect('/');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Donators Page | Admin</title>
    <!-- Bootstrap core CSS-->
    <link href="<?= LINK_PREFIX. 'assets/resx/vendor/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="<?= LINK_PREFIX .'assets/resx/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="<?= LINK_PREFIX .'assets/resx/vendor/datatables/dataTables.bootstrap4.css'; ?>" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?= LINK_PREFIX .'assets/css/sb-admin.css' ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?= LINK_PREFIX .'assets/css/gallery-clean.css' ?>">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="">Art4Dev</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="<?= ADMIN_PREFIX .'dashboard' ?>">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Exhibitors">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX .'exhibitors'?>">
                    <i class="fa fa-fw fa-user-o"></i>
                    <span class="nav-link-text">Exhibitors</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Donators">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX .'donators'; ?>">
                    <i class="fa fa-fw fa-money"></i>
                    <span class="nav-link-text">Donators</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Logout">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX. 'logout'; ?>">
                    <i class="fa fa-fw fa-close"></i>
                    <span class="nav-link-text">Logout</span>
                </a>
            </li>

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
                        </span>
                    <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">New Messages:</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <strong>David Miller</strong>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome!
                            These messages clip off when they reach the end of the box so they don't overflow over to
                            the sides!
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <strong>Jane Smith</strong>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00
                            instead of 4:00. Thanks!
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <strong>John Doe</strong>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">I've sent the final files over to you for review. When
                            you're able to sign off of them let me know and we can discuss distribution.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item small" href="#">View all messages</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-bell"></i>
                    <span class="d-lg-none">Alerts
              <span class="badge badge-pill badge-warning">6 New</span>
                        </span>
                    <span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">New Alerts:</h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                            <span class="text-success">
                <strong>
                  <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">This is an automated server response message. All systems
                            are online.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                            <span class="text-danger">
                <strong>
                  <i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
              </span>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">This is an automated server response message. All systems
                            are online.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                            <span class="text-success">
                <strong>
                  <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
                        <span class="small float-right text-muted">11:21 AM</span>
                        <div class="dropdown-message small">This is an automated server response message. All systems
                            are online.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item small" href="#">View all alerts</a>
                </div>
            </li>
            <li class="nav-item">
                <form class="form-inline my-2 my-lg-0 mr-lg-2">
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Search for...">
                        <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
                    </div>
                </form>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= ADMIN_PREFIX. 'logout'; ?>">
                    <i class="fa fa-fw fa-sign-out"></i>Logout</a>
            </li>
        </ul>
    </div>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?= ADMIN_PREFIX .'dashboard' ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item active"><a href="<?= ADMIN_PREFIX .'donators' ?>">Donators</a></li>
        </ol>
        <!-- Icon Cards-->
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <? if ($ACCEPT_DONOR && isset($SUCCESS)) { ?><label
                        class="label label-<?= $SUCCESS ? 'danger' : 'success' ?>"><?= $ERRMSG ?></label>
                    <? } ?>
                    <? if($Class->allDonators()) {  ?>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <th style="width: 1px;">S/N</th>
                            <th>Full Name</th>
                            <th>Email Address</th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Date Registered</th>
                            <th>Date Activated</th>
                            <th>action</th>
                            </thead>
                            <tbody>
                            <? $n=1; foreach ($Class->allDonators() as $donator) : ?>
                                <tr>
                                    <td><?= $n++; ?></td>
                                    <td><?= $donator->fullname; ?></td>
                                    <td><?= $donator->useremail; ?></td>
                                    <td><?= $donator->phone; ?></td>
                                    <td> <span class="label label-<?= $donator->status == 1 ? 'success' : ($donator->status == 0 ? 'danger' : '') ?>">
                                            <?= $donator->status == 1 ? 'Activated' : ($donator->status == 0 ? 'Not Activated' : ''); ?></span>
                                    </td>
                                    <td><?= date('M d Y', strtotime($donator->date_created)); ?> <br>
                                        <small class="text text-danger"><?= $Class->gen->prettyDate($donator->date_created) ?></small>
                                    </td>
                                    <td><? if ($donator->activate_on) : ?><?= date('M j Y', strtotime($donator->activate_on)); ?> <br>
                                            <small class="text text-success"> <?= $Class->gen->prettyDate($donator->activate_on); ?></small>
                                        <? endif; ?>
                                    </td>
                                    <td><a class="btn btn-sm btn-success" href="<?= ADMIN_PREFIX ?>view-details/<?= $donator->id; ?>">View Details</a></td>
                                </tr>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    <? } else { ?>
                        <div class="text-center no-result wow animated fadeInDown">
                            <span> <i class="fa fa-bullhorn fa-4x fa-fw"></i>No Result to Display, No Application Yet</span>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>

        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Art4Dev 2018</small>
                </div>
            </div>
        </footer>
    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.tz-gallery');
    </script>
</body>

</html>