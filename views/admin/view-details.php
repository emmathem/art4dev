<?php
/**
 * @Author
 * Falua Temitope Oyewole.
 * faluatemitopeo@gmail.com
 * Web Developer
 * Date: 4/10/2018
 */
?>

<? require_once __DIR__ .'/../../lib/Helpers/admin-helper.php';
require_once __DIR__ .'/../../lib/Helpers/include-linkbuilder.php';
$EXHIBITOR = $Class->exhibitorDetails($_GET['id']);

$DONOR = $Class->donorDetails($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Exhibitor Page | Admin</title>
    <!-- Bootstrap core CSS-->
    <link href="<?= LINK_PREFIX. 'assets/resx/vendor/bootstrap/css/bootstrap.min.css' ?>" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="<?= LINK_PREFIX .'assets/resx/vendor/font-awesome/css/font-awesome.min.css' ?>" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="<?= LINK_PREFIX .'assets/resx/vendor/datatables/dataTables.bootstrap4.css'; ?>" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="<?= LINK_PREFIX .'assets/css/sb-admin.css' ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?= LINK_PREFIX.'assets/css/gallery-clean.css' ?>">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="">Art4Dev</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="<?= ADMIN_PREFIX .'dashboard' ?>">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Exhibitors">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX .'exhibitors'?>">
                    <i class="fa fa-fw fa-user-o"></i>
                    <span class="nav-link-text">Exhibitors</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Donators">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX .'donators'; ?>">
                    <i class="fa fa-fw fa-money"></i>
                    <span class="nav-link-text">Donators</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Logout">
                <a class="nav-link collapsed" href="<?= ADMIN_PREFIX. 'logout'; ?>">
                    <i class="fa fa-fw fa-close"></i>
                    <span class="nav-link-text">Logout</span>
                </a>
            </li>

        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item"><a href="<?= ADMIN_PREFIX .'exhibitors'; ?>">Exhibitors</a></li>
            <li class="breadcrumb-item active"><a href="">View Details</a></li>
        </ol>
        <!-- Icon Cards-->
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <?= null; ?>
                    <?  if($EXHIBITOR): ?>
                        <? foreach ($EXHIBITOR as $exhibitor) : ?>
                            <div class="art-works">
                                <? if($exhibitor->art_work) { ?>
                                <h4>Select Art Work to view:</h4>
                                <ul class="tz-gallery list-inline nostyle">
                                    <? foreach (json_decode($exhibitor->art_work) as $img) : ?>
                                    <li class="thumbnail">
                                        <a class="lightbox" href="<?= LINK_PREFIX .'artworks/exhibitors/'. $img ?>">
                                            <img src="<?= LINK_PREFIX .'artworks/exhibitors/'. $img; ?>" alt="art work">
                                        </a>
                                    </li>
                                    <? endforeach; ?>
                                </ul>
                                <? } else { ?>
                                    <h5>No Art Work Uploaded</h5>
                                <? } ?>
                            </div>
                            <div class="col-md-7">
                                <table class="table table-bordered table-condensed reduced">
                                    <tr>
                                        <td>Organization Name</td>
                                        <td><label class="label label-success"><?= $exhibitor->organization; ?></label></td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td>Contact</td>
                                        <td><?= $exhibitor->contact_name; ?> | <?= $exhibitor->phone_no; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email Address</td>
                                        <td><label class="label label-success"><?= $exhibitor->useremail; ?></label></td>
                                    </tr>
                                    <tr>
                                        <td>Interest</td>
                                        <td><?= $exhibitor->interest; ?></td>
                                    </tr>
                                    <tr>
                                        <td>3-Day Booth Allocation</td>
                                        <td><?= $exhibitor->booth_allocation; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Number of Booth</td>
                                        <td><?= $exhibitor->no_of_booth; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td><span class="label label-<?= $exhibitor->status == 1 ? 'success' : ($exhibitor->status == 0 ? 'danger' : '') ?>">
                                            <?= $exhibitor->status == 1 ? 'Activated' : ($exhibitor->status == 0 ? 'Not Activated' : ''); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Profile</td>
                                        <td><?= $exhibitor->about_me; ?></td>
                                    </tr>
                                    <? if ($exhibitor->resume_doc) : ?>
                                    <tr>
                                        <td>Exhibitor Resume</td>
                                        <td><?= $exhibitor->resume_doc ?></td>
                                    </tr>
                                    <? endif; ?>
                                    <? if ($exhibitor->activate_on) : ?>
                                    <tr>
                                        <td>Activated On</td>
                                        <td>
                                            <?= date('M d Y', strtotime($exhibitor->activate_on)); ?>
                                            (<small class="text text-success"><?= $Class->gen->prettyDate($exhibitor->activate_on); ?></small>)
                                        </td>
                                    </tr>
                                    <? endif; ?>
                                    <tr>
                                        <td>Date Registered</td>
                                        <td>
                                            <?= date('M d Y', strtotime($exhibitor->date_created)); ?>
                                           (<small class="text text-warning"><?= $Class->gen->prettyDate($exhibitor->date_created); ?></small>)
                                        </td>
                                    </tr>
                                </table>

                                <div class="form-group text-center">
                                    <? if($exhibitor->status == 0) { ?>
                                        <a href="<?= ADMIN_PREFIX .'exhibitors?accept='.$exhibitor->id;?>" class="btn btn-success btn-sm">Approve</a>
                                        <a href="<?= ADMIN_PREFIX .'exhibitors' ?>" class="btn btn-danger btn-sm">Decline</a>
                                    <? } else { ?> <label class="label label-info">Thank you. Already Activated!</label> <? } ?>

                                </div>
                            </div>
                            <!--<div class="col-md-4">Picture</div>-->

                        <? endforeach; ?>
                    <? endif; ?>

                    <?  if($DONOR) : ?>
                        <? foreach ($DONOR as $donor) : ?>
                            <div class="art-works">
                                <? if($donor->art_work) { ?>
                                    <h4>Select Art Work to view:</h4>
                                    <ul class="tz-gallery list-inline nostyle">
                                        <? foreach (json_decode($donor->art_work) as $img) : ?>
                                            <li class="thumbnail">
                                                <a class="lightbox" href="<?= LINK_PREFIX .'artworks/donators/'. $img ?>">
                                                    <img src="<?= LINK_PREFIX .'artworks/donators/'. $img; ?>" alt="art work">
                                                </a>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? } else { ?>
                                    <h5>No Art Work Uploaded</h5>
                                <? } ?>
                            </div>
                            <div class="col-md-7">
                                <table class="table table-bordered table-condensed reduced">
                                    <tr>
                                        <td>FullName</td>
                                        <td><?= $donor->fullname; ?></td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td>Email Address</td>
                                        <td><label class="label label-success"><?= $donor->useremail; ?></label></td>
                                    </tr>
                                    <tr>
                                        <td>Phone Number</td>
                                        <td><?= $donor->phone; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td><?= $donor->location; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Profile</td>
                                        <td><?= $donor->about_me; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td><span class="label label-<?= $donor->status == 1 ? 'success' : ($donor->status == 0 ? 'danger' : '') ?>">
                                            <?= $donor->status == 1 ? 'Activated' : ($donor->status == 0 ? 'Not Activated' : ''); ?></td>
                                    </tr>

                                    <? if ($donor->activate_on) : ?>
                                        <tr>
                                            <td>Activated On</td>
                                            <td>
                                                <?= date('M d Y', strtotime($donor->activate_on)); ?>
                                                (<small class="text text-success"><?= $Class->gen->prettyDate($donor->activate_on); ?></small>)
                                            </td>
                                        </tr>
                                    <? endif; ?>
                                    <tr>
                                        <td>Date Registered</td>
                                        <td>
                                            <?= date('M d Y', strtotime($donor->date_created)); ?>
                                            (<small class="text text-warning"><?= $Class->gen->prettyDate($donor->date_created); ?></small>)
                                        </td>
                                    </tr>
                                </table>

                                <div class="form-group text-center">
                                    <? if($donor->status == 0) { ?>
                                        <a href="<?= ADMIN_PREFIX .'donators?accept_donor='. $donor->id;?>" class="btn btn-success btn-sm">Activate</a>
                                        <a href="<?= ADMIN_PREFIX .'donators'; ?>" class="btn btn-danger btn-sm">Decline</a>
                                    <? } else { ?> <label class="label label-info">Thank you. Already Activated!</label> <? } ?>

                                </div>
                            </div>
                            <!--<div class="col-md-4">Picture</div>-->

                        <? endforeach; ?>
                    <? endif; ?>

                </div>
            </div>
        </div>

        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Art4Dev 2018</small>
                </div>
            </div>
        </footer>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
</body>

</html>
