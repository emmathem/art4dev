<?php
/**
 * @Author
 * Falua Temitope Oyewole.
 * faluatemitopeo@gmail.com
 * Web Developer
 * Date: 4/27/2018
 */
?>
<?
session_start();
$title = ucfirst(basename($_SERVER['SCRIPT_NAME'], '.php'));
$title = str_replace(["_", "-"], " ", $title);
if (strtolower($title) == 'portfolio') {
    $title = 'Dashboard';
}
$title = ucwords($title);
require_once __DIR__ . '/../../lib/Helpers/include-linkbuilder.php';
require_once __DIR__ . '/../../lib/Classes/Participant.php';

$USER = new Participant();
if (!$USER->is_logged_in()) {
    $USER->redirect('/login');
}

$stmt = $USER->runQuery("SELECT * FROM exhibitors WHERE id=:id LIMIT 1");
$stmt->bindValue("id", $_SESSION['userSession']);
//$stmt->execute(array(":id" => $_SESSION['userSession']));
$stmt->execute();
$user = $stmt->fetch(PDO::FETCH_OBJ);
//var_dump($row); die();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title . ' &mdash; ' . $user->contact_name . 'Art4Dev' ?> </title>
    <link rel="stylesheet" href="<?= LINK_PREFIX . 'assets/css/w3.css' ?>">
    <link rel="stylesheet" href="<?= LINK_PREFIX . 'assets/css/font-awesome/css/font-awesome.min.css' ?>">
    <link rel="stylesheet" href="<?= LINK_PREFIX . 'assets/css/portfolio.css' ?>">
    <link rel="stylesheet" href="<?= LINK_PREFIX . 'assets/css/bootstrap.min.css' ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu|Titillium+Web" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="<?= LINK_PREFIX.'assets/css/gallery-clean.css' ?>">
</head>
<body>
<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left side-bar" style="" id="mySidebar">
    <div class="w3-container profile-menu">
        <a href="#" onclick="w3_close()" class="w3-hide-large w3-right w3-jumbo w3-padding w3-hover-grey"
           title="close menu">
            <i class="fa fa-remove"></i>
        </a>
        <div class="w3-center">
            <? if ($user->photo) { ?>
                <img align="top" class="img-responsive img-circle" src="../../uploads/avatar/<?= $user->photo ?>">
            <? } else { ?>
                <img align="top" class="img-responsive img-circle"
                     src="<?= LINK_PREFIX . 'assets/images/icons/default.jpg' ?>">
            <? } ?>
            <!--<span class="user-name"><?php /*echo 'Welcome ' . $user->contact_name; */ ?> </span>-->
        </div>
        <div class="w3-bar-block w3-center">
            <ul>
                <li><a href="<?= LINK_PREFIX .'portfolio' ?>"><i class="fa fa-dashboard fa-fw"></i> Portfolio</a></li>
                <li><a href="<?= LINK_PREFIX .'portfolio#about' ?>" onclick="w3_close()"><i class="fa fa-user fa-fw"></i> About Me</a></li>
                <li><a href="<?= LINK_PREFIX .'portfolio#contact' ?>" onclick="w3_close()"><i class="fa fa-envelope fa-fw"></i> Contact</a></li>
                <li><a href="<?= LINK_PREFIX .'portfolio/update-profile' ?>"><i class="fa fa-edit fa-fw"></i> Update Profile</a></li>
                <li><a href="<?= LINK_PREFIX . 'logout'; ?>" onclick="w3_close()"><i class="fa fa-sign-out fa-fw"></i>
                        Logout</a></li>
            </ul>
            <!-- <a href="#portfolio" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-text-teal"><i class="fa fa-th-large fa-fw w3-margin-right"></i>PORTFOLIO</a>
             <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fa fa-user fa-fw w3-margin-right"></i>ABOUT</a>
             <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fa fa-envelope fa-fw w3-margin-right"></i>CONTACT</a>
             <a href="profile_update/profile.html" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fa fa-upload fa-fw w3-margin-right"></i>UPDATE PROFILE</a>
             <a href="logout" onclick="w3_close()" class="w-bar-item w3-button w3-padding"><i class="fa fa-close fa-fw"></i> Logout</a>-->
        </div>
        <div class="copyright">
            Copyright <?= date('Y'); ?> &copy; Art4Dev.
        </div>
    </div>
</nav>

