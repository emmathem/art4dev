<? include 'view-stubs/header.php';
require_once __DIR__ . '/../lib/Helpers/gallery-helper.php';
$EX_DETAILS = $Cl->getExhibitorsDetails($_GET['slug']);
?>
<section class="nav-cover"></section>
<section class="exhibitor-page">
    <div class="inner-exhibitor">
        <?= null; ?>
        <? if ($EX_DETAILS) { ?>
            <? foreach ($EX_DETAILS as $ex) : ?>
                <div class="art-works">
                    <h4>View Art Work:</h4>
                    <ul class="tz-gallery list-inline nostyle">
                        <? foreach (json_decode($ex->art_work) as $art): ?>
                            <li class="thumbnail">
                                <a class="lightbox" href="<?= LINK_PREFIX . 'artworks/exhibitors/' . $art ?>">
                                    <img class="_img" src="<?= LINK_PREFIX . 'artworks/exhibitors/' . $art; ?>"
                                         alt="art work">
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-6 details">
                    <h3>The Gallery of <?= $ex->organization; ?></h3>
                    <div class="underline"></div>
                    <div class="more-details">
                    <span>
                        Artist Name: <strong><?= $ex->contact_name; ?></strong> <br>
                        <strong><u>About</u></strong> <br>
                            <?= $ex->about_me; ?>
                        </span>
                    </div>
                </div>
            <? endforeach; ?>
        <? } else { ?>
            <div class="label label-warning">
                <h4>No Detail Found!</h4>
            </div>
        <? } ?>
    </div>
</section>
<? include 'view-stubs/footer.php' ?>
