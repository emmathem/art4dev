<? include __DIR__ . '/view-stubs/port-header.php'; ?>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer"
         title="close side menu" id="myOverlay"></div>

    <!-- !PAGE CONTENT! -->
    <div class="w3-main main-content">
    <!-- Header -->
    <header id="portfolio">
        <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i
                    class="fa fa-bars"></i></span>
        <div class="w3-container dashboard-head">
            <h3>Welcome to your Dashboard <?= $user->organization; ?></h3>
            <hr>
        </div>
    </header>
    <div class="w3-container gallery-show">
        <h3>Recent Uploads</h3>
        <ul class="tz-gallery list-inline">
            <?  foreach (json_decode($user->art_work) as $img) :  ?>
            <li><a class="lightbox" href="<?= LINK_PREFIX .'artworks/exhibitors/'. $img  ?>">
                    <img src="<?= LINK_PREFIX .'artworks/exhibitors/'. $img;  ?>" alt="art work">
                </a>
            </li>
            <? endforeach; ?>
        </ul>
    </div>
    <div class="w3-container w3-padding-large about-me">
        <h3>About Me</h3>
        <hr>
        <h4>Technical Skills</h4>
        <!-- Progress bars / Skills -->
        <p>Photography</p>
        <div class="w3-grey">
            <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:95%">95%</div>
        </div>
        <p>Pottery</p>
        <div class="w3-grey">
            <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:85%">85%</div>
        </div>
        <p>Weaving</p>
        <div class="w3-grey">
            <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:80%">80%</div>
        </div>
        <p>
            <button class="w3-button w3-dark-grey w3-padding-large w3-margin-top w3-margin-bottom">
                <i class="fa fa-download w3-margin-right"></i>Download Resume
            </button>
        </p>
    </div>
<? include __DIR__ . '/view-stubs/port-footer.php'; ?>