-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 30, 2018 at 06:48 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `art4dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(60) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userPass` varchar(100) NOT NULL,
  `tokenCode` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userEmail` (`userEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`userID`, `firstName`, `lastName`, `userEmail`, `userPass`, `tokenCode`, `date_created`) VALUES
(1, 'Action', 'Nid', 'actionnid@gmail.com', '$2y$10$p3JNXvE8vCteNgecO1UFKebSKRaodTKwoZK/50w8sFaB/.LEjBA/q', '2de01b9310e1c74db393059ffa282f57', '2018-04-10 18:34:56'),
(2, 'Temitope', 'Action', 'faluatemitopeo@gmail.com', '$2y$10$Ov6zxFTggmXAVIwJRsjUyO6H.Orl5qJXsOmUj4ZzXrttAr0Uwlh2O', '1f45847e8de5124c8d9f4d84301ebacf', '2018-04-17 12:35:28');

-- --------------------------------------------------------

--
-- Table structure for table `donators`
--

DROP TABLE IF EXISTS `donators`;
CREATE TABLE IF NOT EXISTS `donators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(300) NOT NULL,
  `useremail` varchar(300) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `location` varchar(300) NOT NULL,
  `other_info` varchar(300) NOT NULL,
  `art_work` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `activate_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exhibitors`
--

DROP TABLE IF EXISTS `exhibitors`;
CREATE TABLE IF NOT EXISTS `exhibitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization` varchar(300) NOT NULL,
  `slug` varchar(300) NOT NULL,
  `photo` varchar(300) DEFAULT NULL,
  `contact_name` varchar(300) NOT NULL,
  `useremail` varchar(300) NOT NULL,
  `username` varchar(300) NOT NULL,
  `userpass` varchar(300) NOT NULL,
  `phone_no` varchar(300) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `interest` varchar(300) NOT NULL,
  `booth_allocation` varchar(300) NOT NULL,
  `no_of_booth` varchar(30) NOT NULL,
  `about_me` text NOT NULL,
  `skills` text,
  `resume_doc` varchar(300) DEFAULT NULL,
  `art_work` text,
  `location` varchar(300) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `activate_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exhibitors`
--

INSERT INTO `exhibitors` (`id`, `organization`, `slug`, `photo`, `contact_name`, `useremail`, `username`, `userpass`, `phone_no`, `gender`, `interest`, `booth_allocation`, `no_of_booth`, `about_me`, `skills`, `resume_doc`, `art_work`, `location`, `status`, `date_created`, `activate_on`) VALUES
(1, 'IJO AYO LIMITED', 'ijo-ayo-limited', NULL, 'Ayo Albano', 'ayo@gmail.com', 'ayo-albano-87', '$2y$10$OXq7CnSbOk99W5FG3fLP8.ki9hzGvc52t/7dngf24xkZ1MfirXP2i', NULL, NULL, 'Others', 'Outdoor(200)', '2', 'I am a good artist', NULL, NULL, '[\"ayo-albano-87-15239677950.jpg\",\"ayo-albano-87-152396779501.jpg\",\"ayo-albano-87-1523967795012.jpg\"]', NULL, 1, '2018-04-17 13:23:15', '2018-04-17 14:01:32'),
(2, 'ALAPO MEJI ART', 'alapo-meji-art', NULL, 'Alapo Art', 'faluatemitopeo@gmail.com', 'alapo-art-19', '$2y$10$00sVmZQPF8UOtDCtdw30ROHwkur4y9C8UhgYdZExnMg24F3jBvSLm', NULL, NULL, 'Food Vendor', 'Outdoor(200)', '1', 'I am just testing the gallery', NULL, NULL, '[\"alapo-art-19-15239725970.jpg\",\"alapo-art-19-152397259701.png\",\"alapo-art-19-1523972597012.jpg\"]', NULL, 1, '2018-04-17 14:43:17', '2018-04-17 14:44:04'),
(3, 'ANOTHER ART COMPANY', 'another-art-company', NULL, 'Another Sule', 'another@gmail.com', 'another-su-44', '$2y$10$qcOi7kTcjN/bFzSJ9ZECJeqpE863/EE6eiGHV/auet0TJgB4HKtB.', NULL, NULL, 'Food Vendor', 'Outdoor(200)', '3', 'I am here\r\n', NULL, NULL, '[\"another-su-44-15239833260.jpeg\",\"another-su-44-152398332601.jpeg\",\"another-su-44-1523983326012.jpeg\",\"another-su-44-15239833260123.jpeg\"]', NULL, 1, '2018-04-17 17:42:06', '2018-04-17 17:43:46'),
(7, 'LODUD', 'lodud', NULL, 'lodjd', 'ldo@gm.com', 'lodjd-91', '$2y$10$IYPxuIaB8mYq1FFhCo.HF.eHAsMLgLu5ZkUXegu4tkmenq5gk2XwC', NULL, NULL, 'Arts and Crafts', 'Indoor(60)', '1', 'I amhe', NULL, NULL, '[\"lodjd-91-15239845510.jpg\",\"lodjd-91-152398455101.jpg\"]', NULL, 1, '2018-04-17 18:02:31', '2018-04-29 20:47:39'),
(9, 'TEST NEW ART', 'test-new-art', NULL, 'Test Ayo', 'atu@gmail.com', 'test-ayo-44', '$2y$10$jBXYPlriKS4QvK2DzURvouBxXgzmkUkOuzrUwrv3AN5wIQ8Sgp/iG', '08076674678', NULL, 'Pottery', 'Indoor (#100,000)', '2', 'I have a good profile', NULL, NULL, '[\"test-ayo-44-15250708120.png\",\"test-ayo-44-152507081201....jpg\"]', NULL, 0, '2018-04-30 07:46:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `e_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `e_id` (`e_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `e_id`, `created_at`) VALUES
(1, 1, '2018-04-17 14:01:33'),
(2, 2, '2018-04-17 14:44:05'),
(3, 3, '2018-04-17 17:43:48'),
(4, 7, '2018-04-28 22:26:27'),
(5, 7, '2018-04-29 20:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
CREATE TABLE IF NOT EXISTS `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(300) NOT NULL,
  `fullname` varchar(300) NOT NULL,
  `useremail` varchar(300) NOT NULL,
  `userpass` varchar(300) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `service_needed` varchar(300) NOT NULL,
  `other_info` text NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `exhibitors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
